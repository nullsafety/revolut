package app.revolut.testtask.data.model

data class Currencies(
        var base: String,
        var date: String,
        var rates: HashMap<String, String>
)