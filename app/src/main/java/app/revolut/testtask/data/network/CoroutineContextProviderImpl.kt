package app.revolut.testtask.data.network

import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.CoroutineContext


class CoroutineContextProviderImpl : CoroutineContextProvider {
    override val main: CoroutineContext by lazy { Dispatchers.Main }
    override val io: CoroutineContext by lazy { Dispatchers.IO }
}