package app.revolut.testtask.data.repository.currencies_api

import app.revolut.testtask.data.model.Currencies
import app.revolut.testtask.utils.getKoinInstance
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrenciesApiService {
    @GET("latest")
    suspend fun getCurrencies(@Query("base") currencyName: String): Currencies
}

fun getCurrenciesApiService(): CurrenciesApiService {
    val retrofit = getKoinInstance<Retrofit>()
    return retrofit.create(CurrenciesApiService::class.java)
}
