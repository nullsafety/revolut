package app.revolut.testtask.data.network

import kotlin.coroutines.CoroutineContext


interface CoroutineContextProvider {
    val main: CoroutineContext
    val io: CoroutineContext
}