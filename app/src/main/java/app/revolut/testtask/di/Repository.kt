package app.revolut.testtask.di

import app.revolut.testtask.data.repository.currencies_api.getCurrenciesApiService
import org.koin.core.module.Module
import org.koin.dsl.module

val repositoryModule: Module = module {
    single { getCurrenciesApiService() }
}