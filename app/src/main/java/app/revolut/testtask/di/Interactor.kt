package app.revolut.testtask.di

import app.revolut.testtask.domain.interactors.CurrencyInteractor
import app.revolut.testtask.domain.interactors.CurrencyInteractorImpl
import org.koin.core.module.Module
import org.koin.dsl.module

val interactorModule: Module = module {
    single {CurrencyInteractorImpl() as CurrencyInteractor}
}