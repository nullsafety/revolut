package app.revolut.testtask.di

import app.revolut.testtask.data.const.BASE_URL
import app.revolut.testtask.data.network.CoroutineContextProvider
import app.revolut.testtask.data.network.CoroutineContextProviderImpl
import org.koin.core.module.Module
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val networkModule: Module = module {
    single { Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build() }

    single { CoroutineContextProviderImpl() as CoroutineContextProvider }
}