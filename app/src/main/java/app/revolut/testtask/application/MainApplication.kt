package app.revolut.testtask.application

import android.app.Application
import app.revolut.testtask.di.interactorModule
import app.revolut.testtask.di.networkModule
import app.revolut.testtask.di.repositoryModule
import org.koin.core.context.startKoin

class MainApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin { modules(listOf(networkModule, repositoryModule, interactorModule)) }
    }
}