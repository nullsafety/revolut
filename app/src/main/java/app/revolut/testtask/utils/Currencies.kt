package app.revolut.testtask.utils

import app.revolut.testtask.data.model.Currencies
import app.revolut.testtask.domain.model.Currency
import java.math.BigDecimal
import java.math.RoundingMode

fun Currencies.toCurrencyList(): ArrayList<Currency> {
    val arrayList = ArrayList<Currency>()
    this.rates.forEach {
        if (currencyImagesMap.containsKey(it.key) && currencyDescriptionMap.containsKey(it.key))
            arrayList.add(Currency(it.key, currencyImagesMap[it.key]!!, currencyDescriptionMap[it.key]!!, it.value, it.key == this.base))
    }
    return arrayList
}