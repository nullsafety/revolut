package app.revolut.testtask.utils

import android.text.Editable
import android.text.TextWatcher

fun textWatcher(onTextChanged: (changedRate: CharSequence?) -> Unit): TextWatcher {
    return object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {

        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

        }

        override fun onTextChanged(changedRate: CharSequence?, p1: Int, p2: Int, p3: Int) {
            onTextChanged.invoke(changedRate)
        }
    }
}