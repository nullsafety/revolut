package app.revolut.testtask.utils

import java.math.BigDecimal
import java.math.RoundingMode

fun String.toCurrency(multiply: BigDecimal = BigDecimal(1.0)) : String {
    return "${(this.toBigDecimal() * multiply)
            .setScale(4, RoundingMode.HALF_UP)
            .stripTrailingZeros()}"
}