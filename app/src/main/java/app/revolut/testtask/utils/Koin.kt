package app.revolut.testtask.utils

import org.koin.core.KoinComponent
import org.koin.core.inject

inline fun <reified T> getKoinInstance(): T = object : KoinComponent {
    val value by inject<T>()
}.value