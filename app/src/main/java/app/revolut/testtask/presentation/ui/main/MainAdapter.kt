package app.revolut.testtask.presentation.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.RecyclerView
import app.revolut.testtask.R
import app.revolut.testtask.domain.model.Currency
import app.revolut.testtask.utils.currencyImagesMap
import app.revolut.testtask.utils.textWatcher
import app.revolut.testtask.utils.toCurrency
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*
import kotlin.collections.ArrayList


class MainAdapter(private val onItemListener: OnItemListener)
    : RecyclerView.Adapter<MainAdapter.ViewHolder>() {

    private var baseRate = BigDecimal(1.0)
    private var list: ArrayList<Currency> = ArrayList()

    fun updateItems(newList: ArrayList<Currency>, currency: Currency) {
        list.clear()
        list.addAll(newList)
        list.add(0, currency)

        list.forEachIndexed { index, it ->
            if (index > 0) notifyItemChanged(index, it)
        }
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currency = list[position]
        holder.name.text = currency.name
        holder.description.text = currency.description
        holder.image.setImageDrawable(currencyImagesMap[currency.name]?.let {
            AppCompatResources.getDrawable(holder.name.context, it)
        })
        holder.rootView.setOnClickListener {
            currency.rate = holder.rate.text.toString()
            if (position != 0) setCurrent(position, currency)
        }

        if (currency.isBase) holder.setBase(currency)
        else holder.setNotBase(currency, position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val root = LayoutInflater.from(parent.context).inflate(R.layout.currency_item, parent, false)
        return ViewHolder(root)
    }

    private fun setCurrent(position: Int, currency: Currency) {
        list[position].isBase = true
        list[0].isBase = false
        baseRate = list[position].rate.toBigDecimal()
        Collections.swap(list, 0, position)

        list.forEachIndexed { index, itCurrency ->
            try {
                notifyItemChanged(index, itCurrency)
            } catch (e: IllegalStateException) {

            }
        }
        onItemListener.onSelected(currency, position)
    }

    inner class ViewHolder(root: View) : RecyclerView.ViewHolder(root) {
        val rootView = root
        val name: TextView = root.findViewById(R.id.name)
        val description: TextView = root.findViewById(R.id.description)
        val rate: EditText = root.findViewById(R.id.rate)
        val image: ImageView = root.findViewById(R.id.image)

        fun setBase(currency: Currency) {
            rate.setText(currency.rate.toCurrency())
            rate.isFocusableInTouchMode = true
            rate.requestFocus()
            rate.addTextChangedListener(textWatcher {
                if (rate.isFocused) {
                    baseRate = try {
                        it?.toString()?.toBigDecimal() ?: BigDecimal(0.0)
                    } catch (exception: Exception) {
                        BigDecimal(0.0)
                    }
                    try {
                        notifyItemRangeChanged(1, list.size)
                    } catch (e: IllegalStateException) {

                    }
                }
            })
        }

        fun setNotBase(currency: Currency, position: Int) {
            rate.setText(currency.rate.toCurrency(baseRate))
            rate.isFocusableInTouchMode = true
            rate.setOnFocusChangeListener { _, hasFocus ->
                currency.rate = rate.text.toString()
                if (hasFocus) setCurrent(position, currency)
            }
        }
    }
}