package app.revolut.testtask.presentation.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import app.revolut.testtask.R
import app.revolut.testtask.domain.model.Currency
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), LifecycleOwner {

    lateinit var adapter: MainAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        val viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        adapter = MainAdapter(object : OnItemListener {
            override fun onSelected(currency: Currency, position: Int) {
                observeCurrentCurrencies(viewModel)
                viewModel.currentCurrency = currency
            }
        })

        recyclerViewCurrencies.layoutManager = LinearLayoutManager(this)
        recyclerViewCurrencies.adapter = adapter

        viewModel.observeCurrencies().observe(this, Observer {
            adapter.updateItems(it, viewModel.currentCurrency)
        })

        viewModel.currencySelected()
    }

    fun observeCurrentCurrencies(viewModel: MainViewModel) {
        viewModel.observeCurrentCurrencies().observe(this, Observer {
            viewModel.observeCurrentCurrencies().removeObservers(this)
        })
    }
}
