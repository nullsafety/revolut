package app.revolut.testtask.presentation.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.revolut.testtask.R
import app.revolut.testtask.data.const.DEFAULT_CURRENCY
import app.revolut.testtask.domain.interactors.CurrencyInteractor
import app.revolut.testtask.domain.model.Currency
import app.revolut.testtask.utils.currencyDescriptionMap
import app.revolut.testtask.utils.toCurrencyList
import org.koin.core.KoinComponent
import org.koin.core.inject


class MainViewModel : ViewModel(), KoinComponent {

    private val currencyInteractor: CurrencyInteractor by inject()

    private val currenciesLiveData: MutableLiveData<ArrayList<Currency>> = MutableLiveData()
    private val currentCurrencyLiveData: MutableLiveData<ArrayList<Currency>> = MutableLiveData()

    var currentCurrency = Currency(DEFAULT_CURRENCY, R.drawable.european_union, currencyDescriptionMap[DEFAULT_CURRENCY]!!, "1", true)
        set(value) {
            field = value
            reloadCurrencies(value)
        }

    fun observeCurrencies(): MutableLiveData<ArrayList<Currency>> {
        loadCurrencies(currentCurrency.name)
        return currenciesLiveData
    }

    fun observeCurrentCurrencies(): MutableLiveData<ArrayList<Currency>> = currentCurrencyLiveData

    fun currencySelected(currency: Currency = this.currentCurrency, amount: Float = 1F) {
        this.currentCurrency = currency.apply { this.rate = amount.toString() }
    }

    private fun loadCurrencies(name: String) {
        currencyInteractor.start(name, {
            currenciesLiveData.value = it.toCurrencyList()
        }, {})
    }

    private fun reloadCurrencies(value: Currency) {
        currencyInteractor.restart(value.name, {
            if (currentCurrencyLiveData.hasActiveObservers())
                currentCurrencyLiveData.postValue(it.toCurrencyList())
            else
                currenciesLiveData.postValue(it.toCurrencyList())
        }, {})
    }

    override fun onCleared() {
        super.onCleared()
        currencyInteractor.finish()
    }
}