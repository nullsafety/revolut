package app.revolut.testtask.presentation.ui.main

import app.revolut.testtask.domain.model.Currency

interface OnItemListener {
    fun onSelected(currency: Currency, position: Int)
}