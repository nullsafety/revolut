package app.revolut.testtask.domain.interactors

import android.util.Log
import app.revolut.testtask.data.const.DEFAULT_CURRENCY
import app.revolut.testtask.data.model.Currencies
import app.revolut.testtask.data.network.CoroutineContextProvider
import app.revolut.testtask.data.repository.currencies_api.getCurrenciesApiService
import kotlinx.coroutines.*
import org.koin.core.KoinComponent
import org.koin.core.inject

class CurrencyInteractorImpl : CurrencyInteractor, KoinComponent {

    private val coroutineContextProvider: CoroutineContextProvider by inject()

    var currencyName: String = DEFAULT_CURRENCY

    @Volatile
    var finish = false

    var job: Job? = null

    override fun start(currencyName: String, action: (currencies: Currencies) -> Unit, error: (throwable: Throwable) -> Unit) {
        this.currencyName = currencyName
        request(action, error)
    }

    override fun restart(currencyName: String, action: (currencies: Currencies) -> Unit, error: (throwable: Throwable) -> Unit) {
        job?.cancel()
        this.currencyName = currencyName
        request(action, error)
    }

    override fun finish() {
        job?.cancel()
    }

    private fun request(action: (currencies: Currencies) -> Unit, errorCallback: (throwable: Throwable) -> Unit) {
        job = GlobalScope.launch(coroutineContextProvider.io) {
            while (!finish) {
                try {
                    val currencies = getCurrenciesApiService().getCurrencies(currencyName)
                    withContext(coroutineContextProvider.main) {
                        Log.e("!!!", currencies.toString())
                        action(currencies)
                    }
                } catch (e: Exception) {
                    withContext(coroutineContextProvider.main) {
                        errorCallback(e)
                    }
                }
                delay(1000)
            }
        }
    }
}

