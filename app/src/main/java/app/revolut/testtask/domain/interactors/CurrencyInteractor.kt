package app.revolut.testtask.domain.interactors

import app.revolut.testtask.data.model.Currencies

interface CurrencyInteractor {

    fun start(currencyName: String, action: (currencies: Currencies) -> Unit, error: (throwable: Throwable) -> Unit)
    fun restart(currencyName: String, action: (currencies: Currencies) -> Unit, error: (throwable: Throwable) -> Unit)
    fun finish()
}