package app.revolut.testtask.domain.model

data class Currency(
        var name: String,
        var image: Int,
        var description: String,
        var rate: String,
        var isBase: Boolean
)

